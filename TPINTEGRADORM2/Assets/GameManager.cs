using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    public static int P1puntos;
    public static int P2puntos;
    public bool P1sumauno;
    public bool P2sumauno;

    void Start()
    {
    }

    void Update()
    {
       SumarPuntos();
       
    }

    public void ComenzarJuego()
    {
                  
     SceneManager.LoadScene("Plataformas");
            
    }   

    public void SumarPuntos()
    {
        if (P1puntos <= 2 && P2puntos <= 2) 
        {
            if (ControlNivel1.GanaRonda == 1)
            {
                P1sumauno = true;
            }
            if (ControlNivel1.GanaRonda == 2)
            {
                P2sumauno = true;
            }

            if (P1sumauno == true)
            {
                P1puntos++;
                ControlNivel1.GanaRonda = 3;
                P1sumauno = false;
            }

            if (P2sumauno == true)
            {
                P2puntos++;
                ControlNivel1.GanaRonda = 3;
                P2sumauno = false;
            }
        }
    }

    // FUNCIONES DEL MENU   
  
    public void MenuPrincipal()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void Salir()
    {
     Application.Quit();
    }

}


