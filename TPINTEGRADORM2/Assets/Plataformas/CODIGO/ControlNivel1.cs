using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ControlNivel1 : MonoBehaviour
{
    public static int P1puntaje;
    public static int P2puntaje;

    // RECURSOS PORTALES
    public static GameObject JugadorUNO;
    public static GameObject JugadorDOS;
    public  Transform Portal1;
    public  Transform Portal2;
    public GameObject Particulas1;
    public GameObject Particulas2;

    // RECURSOS ITEMS
    public GameObject ItemLava;
    private GameObject Objeto;
    public GameObject Poder;
    public GameObject Items1;
    public GameObject Items2;
    public GameObject Items3;
    public GameObject Items4;
    public float ContadorItems;
    public int SelectorItems;
    public bool Crear;
    public bool Crear2;
    
    // RECURSOS INSTANCIADOR 1
    public Vector3 Punto;
    public float PuntoX;
    public float PuntoY;    

    // RECURSOS INSTANCIADOR2
    public Vector3 Punto2;
    public float PuntoX2;
    public float PuntoY2;

    // RECURSOS INSTANCIADOR2
    public Vector3 Punto3;
    public float PuntoX3;
    public bool Crear3;
    public float ContadorLava;

    // RECURSOS INSTANCIADOR2
    public Transform Punto4;  
    public bool Crear4;
    public float ContadorPoder;

    // RECURSOS NIVEL
    public GameObject CartelGana1;
    public GameObject CartelGana2;
    public static int GanaRonda;

    void Start()
    {
        GanaRonda = 0;
        Jugador.Puntaje = 0;
        Jugador2.Puntaje = 0;
        Time.timeScale = 1f;
        
    }

    void Update()
    {
        Portales();
        ControlItems();
        QuienGano();
        QuienGanoJuego();
       
    }

    public void Portales()
    {
        if (Jugador.PosicionTeleport == 1)
        {
        JugadorUNO.transform.position = Portal2.transform.position;
        Jugador.PosicionTeleport = 0;
        }

        if (Jugador.PosicionTeleport == 2)
        {
            JugadorUNO.transform.position = Portal1.transform.position;
            Jugador.PosicionTeleport = 0;
        }

        if (Jugador2.PosicionTeleport == 1)
        {
            JugadorDOS.transform.position = Portal2.transform.position;
            Jugador2.PosicionTeleport = 0;
        }

        if (Jugador2.PosicionTeleport == 2)
        {
            JugadorDOS.transform.position = Portal1.transform.position;
            Jugador2.PosicionTeleport = 0;
        }

        Particulas1.transform.position = Portal1.transform.position;
        Particulas2.transform.position = Portal2.transform.position;
    }
    public void ControlItems()


    {
        ContadorItems = ContadorItems + Time.deltaTime;
        if (ContadorItems > 5)
        {
            Crear = true;
            Crear2 = true;
            ContadorItems = 0;

        } 
        SelectorItems = Random.Range(1,4);
        switch (SelectorItems)
        {
            case 1:
            Objeto = Items1;
            break;
            case 2:
            Objeto = Items2;
            break;
            case 3:
            Objeto = Items3;
            break;
            case 4:
                Objeto = Items4;
                break;
        }

        if (Crear == true)
            CrearItem();
        if (Crear2 == true)
            CrearItem2();

        ContadorLava = ContadorLava + Time.deltaTime;
        if (ContadorLava >= 5)
        {
            Crear3 = true;
            ContadorLava = 0;
        }
        if (Crear3 == true)
            CrearItem3();



        ContadorPoder = ContadorPoder + Time.deltaTime;
        if (ContadorPoder >= 10)
        {
            Crear4 = true;
            ContadorPoder = 0;
        }
        if (Crear4 == true)
            CrearItem4();
    }
    private void CrearItem()
    {
        PuntoX = Random.Range(-28, 31);
        PuntoY = Random.Range(2, 15);
        Punto = new Vector3(PuntoX, PuntoY, 0);
        Instantiate(Objeto, Punto, Quaternion.identity);
        Crear = false;
    }
    private void CrearItem2()
    {
        PuntoX2 = Random.Range(-28, 31);
        PuntoY2 = Random.Range(2, 15);
        Punto2 = new Vector3(PuntoX2, PuntoY2, 0);
        Instantiate(Objeto, Punto2, Quaternion.identity);
        Crear2 = false;
    }
    private void CrearItem3()
    {
        PuntoX3 = Random.Range(-28, 31);

        Punto3 = new Vector3(PuntoX3, 0, 0);
        Instantiate(ItemLava, Punto3, Quaternion.identity);
        Crear3 = false;
    }
    private void CrearItem4()
    {
      
        Instantiate(Poder, Punto4.position, Quaternion.identity);
        Crear4 = false;
    }
    private void QuienGano()
    {
        if (Jugador.Puntaje == 3)
        {
            GanaRonda = 1;
            Jugador.Puntaje = 0;
            Time.timeScale = 0;
        } // SI JUNTA 3 MEDALLAS GANA LA RONDA

        if (Jugador2.Puntaje == 3)
        {
            GanaRonda = 2;
            Jugador2.Puntaje = 0;
            Time.timeScale = 0;
        }// SI JUNTA 3 MEDALLAS GANA LA RONDA


    }
    public void QuienGanoJuego()
    {
        if (GameManager.P1puntos == 3)
        {
            CartelGana1.SetActive(true);                     
            
        }
        else { CartelGana1.SetActive(false); }

        if (GameManager.P2puntos == 3)
        {
            CartelGana2.SetActive(true);
           
        }
        else { CartelGana2.SetActive(false); }
    }    
    public void Menu()
    {
        if (GanaRonda != 0)
        {
            GameManager.P1puntos = 0;
            GameManager.P2puntos = 0;
            SceneManager.LoadScene("Menu");
        }
    }
    public void ReiniciarRonda(InputAction.CallbackContext callbackContext)
    {
        if (GanaRonda == 3 && GameManager.P1puntos <= 3 && GameManager.P2puntos <= 3)
        {
            SceneManager.LoadScene("Plataformas");

        }

        if (GanaRonda == 3 && GameManager.P1puntos == 3 || GameManager.P2puntos == 3)
        {
            GameManager.P1puntos = 0;
            GameManager.P2puntos = 0;
            SceneManager.LoadScene("Plataformas");
        }

    }
}
