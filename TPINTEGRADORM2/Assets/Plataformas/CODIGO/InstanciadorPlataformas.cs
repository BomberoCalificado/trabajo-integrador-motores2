using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorPlataformas : MonoBehaviour
{
    public GameObject Plataformas;
    // RECURSOS INSTANCIADOR 1
    public Vector3 Punto;
    public float PuntoX;
    public float PuntoY;
    public int Azar;

    // RECURSOS INSTANCIADOR2
    public Vector3 Punto2;
    public float PuntoX2;
    public float PuntoY2;
    public int Azar2;

    // RECURSOS INSTANCIADOR3
    public Vector3 Punto3;
    public float PuntoX3;
    public float PuntoY3;
    public int Azar3;

    // RECURSOS INSTANCIADOR4
    public Vector3 Punto4;
    public float PuntoX4;
    public float PuntoY4;
    public int Azar4;

    public float ContadorCreador;

    void Start()
    {
        
    }


    void Update()
    {
        ContadorCreador = ContadorCreador + Time.deltaTime;
        if (ContadorCreador >= 3.5f)
        {
            CrearPlataforma();
            CrearPlataforma2();
            CrearPlataforma3();
            CrearPlataforma4();
            ContadorCreador = 0;
        }
        
        
    }

    private void CrearPlataforma()
    {

        Azar = Random.Range(1,6);
        
        switch (Azar)
        {

                case 1:
                PuntoX = 18;
                PuntoY = 24;
                break;

                case 2:
                PuntoX = 24;
                PuntoY = 24;
                break;

                case 3:
                PuntoX = 18;
                PuntoY = 16;
                break;

                case 4:
                PuntoX = 24;
                PuntoY = 16;
                break;

                case 5:
                PuntoX = 18;
                PuntoY = 8;
                break;

                case 6:
                PuntoX = 24;
                PuntoY = 8;
                break;
        }
        Punto = new Vector3(PuntoX,PuntoY,0);
        Instantiate(Plataformas, Punto, Quaternion.identity);
    }
    private void CrearPlataforma2()
    {
        Azar2 = Random.Range(1, 6);

        switch (Azar2)
        {

            case 1:
                PuntoX2 = -18;
                PuntoY2 = 24;
                break;

            case 2:
                PuntoX2 = -24;
                PuntoY2 = 24;
                break;

            case 3:
                PuntoX2 = -18;
                PuntoY2 = 16;
                break;

            case 4:
                PuntoX2 = -24;
                PuntoY2 = 16;
                break;

            case 5:
                PuntoX2 = -18;
                PuntoY2 = 8;
                break;

            case 6:
                PuntoX2 = -24;
                PuntoY2 = 8;
                break;
        }
        Punto2 = new Vector3(PuntoX2, PuntoY2, 0);
        Instantiate(Plataformas, Punto2, Quaternion.identity);
    }
    private void CrearPlataforma3()
    {

        Azar3 = Random.Range(1, 3);

        switch (Azar3)
        {

            case 1:
                PuntoX3 = 0;
                PuntoY3 = 24;
                break;

            case 2:
                PuntoX3 = 0;
                PuntoY3 = 16;
                break;

            case 3:
                PuntoX3 = 0;
                PuntoY3 = 8;
                break;

            
        }
        Punto3 = new Vector3(PuntoX3, PuntoY3, 0);
        Instantiate(Plataformas, Punto3, Quaternion.identity);
    }
    private void CrearPlataforma4()
    {

        Azar4 = Random.Range(1, 3);

        switch (Azar3)
        {

            case 1:
                PuntoX4 = 0;
                PuntoY4 = 8;
                break;

            case 2:
                PuntoX4 = 8;
                PuntoY4 = 8;
                break;

            case 3:
                PuntoX4 = -8;
                PuntoY4 = 8;
                break;


        }
        Punto4 = new Vector3(PuntoX4, PuntoY4, 0);
        Instantiate(Plataformas, Punto4, Quaternion.identity);
    }
}
