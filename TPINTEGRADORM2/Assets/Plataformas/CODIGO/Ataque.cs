using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ataque : MonoBehaviour
{
    public float Velocidad;
    void Start()
    {

    }


    void Update()
    {
        transform.position = transform.position + new Vector3(1, 0, 0) * Velocidad * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
          //  Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Pared"))
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Pared"))
        {
            Destroy(gameObject);

        }
        if (collision.collider.CompareTag("Jugador"))
        {
           // Destroy(gameObject);

        }
    }
}
