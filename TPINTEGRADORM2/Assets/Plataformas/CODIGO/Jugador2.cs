using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class Jugador2 : MonoBehaviour
{
    public Rigidbody RB;

    // RECURSO SALTO
    public float FuerzaSalto;
    public static bool Suelo;
    public static bool Saltando;
    public RaycastHit InfoRayo;
    public float DistacniaRayo;
    public LayerMask Plataformas;
    public float Gravedad; 
    public int ContadorSaltos;
    public bool PrimerSalto;
    public bool SegundoSalto;

    //RECURSOS MOVIMIENTO
    public PlayerInput Elplayerinpunt;
    public static Vector2 Inpunt;
    public static bool Moviendose;
    public float Fuerza;

    // AGARRE DE PARED
    public static bool AgarrarPared;

    // ROTACION
    public float VelocidadRotacion;

    public static int Puntaje;
    public bool Sumar1;

    // RECURSOS TELETRANSPORTACION
    public static int PosicionTeleport;

    // RECURSOS HABILIDADES
    public static int Poder;
    public Transform UbicacionPlataforma;
    public GameObject PrefabPlataforma;

    // PODER LAVA    
    public Color Lava;
    public Color Normal;
    public float TiempoLava;

    void Start()
    {
        AgarrarPared = false;
        TiempoLava = 0;
        Poder = 0;
        Puntaje = 0;
        Elplayerinpunt = gameObject.GetComponent<PlayerInput>();
        RB = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        MovimientoJugador();
        VerificarSuelo();
        PoderLava();
        VerificarPared();

        ControlNivel1.JugadorDOS = gameObject;

        Inpunt = Elplayerinpunt.actions["Mover2"].ReadValue<Vector2>();
        if (AgarrarPared == false)
        {
            GravedadConstante();
        }
        if (Sumar1 == true)
        {
            Puntaje = Puntaje + 1;
            Sumar1 = false;
        }

    }

    private void MovimientoJugador()
    {

        RB.AddForce(new Vector3(Inpunt.x, 0, 0) * Fuerza);
        if (Inpunt.x != 0)
        {
            Moviendose = true;
        }
        else { Moviendose = false; }

    }
    public void RotarJugadorIzquierda()
    {
        transform.rotation = Quaternion.Euler(0, -90, 0);
    }
    public void RotarJugadorDerecha()
    {
        transform.rotation = Quaternion.Euler(0, 90, 0);
    }
    private void VerificarSuelo()
    {
        Suelo = Physics.Raycast(transform.position, Vector3.down, out InfoRayo, DistacniaRayo, Plataformas);
        if (Suelo == true)
        {
            PrimerSalto = false;
            SegundoSalto = false;
            ContadorSaltos = 0;
            Saltando = false;
        }
    }
    public void Habilidades(InputAction.CallbackContext callbackContextP)
    {
        if (Poder == 1 && callbackContextP.performed)
        {
            Instantiate(PrefabPlataforma, UbicacionPlataforma.position, Quaternion.identity);
            Poder = 0;
        }


    }
    private void VerificarPared()
    {
     
        if (AgarrarPared == true)
        {
            PrimerSalto = false;
            SegundoSalto = false;
            ContadorSaltos = 0;
            Saltando = false;
        }

    }
    public void PoderLava()
    {
        if (Poder == 2)
        {
            TiempoLava = TiempoLava + Time.deltaTime;

            if (TiempoLava > 0.2f && TiempoLava <= 5)
            {
                transform.GetComponent<Renderer>().material.color = Lava;
                gameObject.tag = "Lava";

            }
            if (TiempoLava >= 5)
            {
                gameObject.tag = "Jugador";
                transform.GetComponent<Renderer>().material.color = Normal;
                TiempoLava = 0;
                Poder = 0;
            }
        }

    }
    public void Saltar(InputAction.CallbackContext callbackContext)
    {
        if (PrimerSalto == false && callbackContext.performed)
        {
            RB.AddForce(new Vector3(0, FuerzaSalto, 0), ForceMode.Impulse);
            ContadorSaltos = 1;
            PrimerSalto = true;
            Saltando = true;

            Debug.Log("salte");
        }
        if (SegundoSalto == false && ContadorSaltos == 1 && callbackContext.performed)
        {
            RB.AddForce(new Vector3(0, FuerzaSalto, 0), ForceMode.Impulse);
            SegundoSalto = true;
            Saltando = true;
            ContadorSaltos = 2;
        }


    }
    public void GravedadConstante()
    {
        RB.AddForce(Vector3.down * Gravedad);
    }



    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.CompareTag("Lava"))
        {
            ControlNivel1.GanaRonda = 1;
            Time.timeScale = 0f;
            //Destroy(gameObject);
        }
        if (collision.collider.CompareTag("Pared"))
        {
            AgarrarPared = true;
           
        }
        
        if (collision.collider.CompareTag("Portal1"))
        {
            PosicionTeleport = 1;
        }

        if (collision.collider.CompareTag("Portal2"))
        {
            PosicionTeleport = 2;
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Pared"))
        {
            AgarrarPared = false;

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (Poder == 0 && other.gameObject.CompareTag("Item1"))
        {
            Poder = 1;
        }
        if (other.gameObject.CompareTag("Item2"))
        {
            Sumar1 = true;
        }
        if (Poder == 0 && other.gameObject.CompareTag("Item4"))
        {
            Poder = 2;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * DistacniaRayo);
    }

}
