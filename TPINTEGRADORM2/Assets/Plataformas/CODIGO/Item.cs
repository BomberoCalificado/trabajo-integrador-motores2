using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public float TiempoUso;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TiempoUso = TiempoUso + Time.deltaTime;
        if (TiempoUso >= 6)
        {
            Destroy(gameObject);
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            Destroy(gameObject);
        }
    }
}
