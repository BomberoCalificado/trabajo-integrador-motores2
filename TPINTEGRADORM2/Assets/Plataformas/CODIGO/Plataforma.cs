using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public Rigidbody RB;
    public float Fuerza;
    public float Velocidad;    
    public bool colisiono;
    public float TiempodeUso;
    
    void Start()
    {
        colisiono = false;
        RB = GetComponent<Rigidbody>();
    }


    void Update() 
    {
        
        TiempodeUso = TiempodeUso + Time.deltaTime;
        
        if (TiempodeUso >= 2 && colisiono == true)
        {
            transform.GetComponent<Renderer>().material.color = Color.yellow;
            Desplazar();
        }                
        if (TiempodeUso >= 4f)
        {
         transform.GetComponent<Renderer>().material.color = Color.red;
         Destroy(gameObject,3);
        }
    }

    private void Desplazar()
    {
        RB.AddForce(new Vector3(-Fuerza * Velocidad, 0, 0), ForceMode.Force);
    }
    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            colisiono = true;
        
        }

    }

}
