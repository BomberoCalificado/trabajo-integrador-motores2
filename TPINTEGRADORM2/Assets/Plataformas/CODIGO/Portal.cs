using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public bool Subir;
    public float Limite;
    public float Velocidad;
    
    void Start()
    {
        Limite = transform.position.y; 
    }

    
    void Update()
    {
     Movimiento();
    }

    private void Movimiento()
    {
     
     if (Limite <= 4)
        {
            Subir = true;
        
        }
     if (Limite >= 27)
        {
            Subir = false;
            
        }

     if (Subir == true)
        {
         Limite = Limite + Time.deltaTime;
         transform.position = new Vector3(transform.position.x, Limite , transform.position.z);
        }
     if (Subir == false)
        {
            Limite = Limite - Time.deltaTime;
            transform.position = new Vector3(transform.position.x, Limite , transform.position.z);
        }
    }
}
