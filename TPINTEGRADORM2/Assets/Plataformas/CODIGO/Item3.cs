using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item3 : MonoBehaviour
{
    public float Velocidad;
    void Start()
    {
        
    }

    
    void Update()
    {
        transform.position = transform.position + new Vector3(0, 1, 0) * Velocidad * Time.deltaTime; 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Techo"))
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Techo"))
        {
           Destroy(gameObject);

        }
        if (collision.collider.CompareTag("Jugador"))
        {
            Destroy(gameObject);

        }
    }
}
