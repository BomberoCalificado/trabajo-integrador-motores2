using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlP1 : MonoBehaviour
{

    public Animator ComponenteAnimator;


    void Start()
    {

    }


    void Update()
    {
        VerificarMovimiento();
        VerificarSalto();
        VerificarSuelo();
        AgarrarParedes();
    }

    public void VerificarMovimiento()
    {
        if (Jugador.Moviendose == true)
            ComponenteAnimator.SetBool("Correr", true);
        else { ComponenteAnimator.SetBool("Correr", false); }
    }
    public void VerificarSuelo()
    {
        if (Jugador.Suelo == true)
            ComponenteAnimator.SetBool("TocarSuelo", true);
        else { ComponenteAnimator.SetBool("TocarSuelo", false); }
    }
    public void VerificarSalto()
    {
        if (Jugador.Saltando == true)
            ComponenteAnimator.SetBool("Saltar", true);
        else { ComponenteAnimator.SetBool("Saltar", false); }
    }
    public void AgarrarParedes()
    {
        if (Jugador.AgarrarPared == true)
            ComponenteAnimator.SetBool("TocarPared", true);
        else { ComponenteAnimator.SetBool("TocarPared", false); }
    }
    
}