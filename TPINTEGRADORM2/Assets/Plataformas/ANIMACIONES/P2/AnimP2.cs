using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimP2 : MonoBehaviour
{
    public Animator ComponenteAnimator;


    void Start()
    {

    }


    void Update()
    {
        VerificarMovimiento();
        VerificarSalto();
        VerificarSuelo();
        AgarrarParedes();
    }

    public void VerificarMovimiento()
    {
        if (Jugador2.Moviendose == true)
            ComponenteAnimator.SetBool("Correr", true);
        else { ComponenteAnimator.SetBool("Correr", false); }
    }
    public void VerificarSuelo()
    {
        if (Jugador2.Suelo == true)
            ComponenteAnimator.SetBool("TocarSuelo", true);
        else { ComponenteAnimator.SetBool("TocarSuelo", false); }
    }
    public void VerificarSalto()
    {
        if (Jugador2.Saltando == true)
            ComponenteAnimator.SetBool("Saltar", true);
        if (Jugador2.Saltando == false)
        ComponenteAnimator.SetBool("Saltar", false); 
    }
    public void AgarrarParedes()
    {
        if (Jugador2.AgarrarPared == true)
            ComponenteAnimator.SetBool("TocarPared", true);
        else { ComponenteAnimator.SetBool("TocarPared", false); }
    }
}
