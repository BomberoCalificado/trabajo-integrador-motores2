using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPuntos : MonoBehaviour
{
    public Text P1puntajeTexto;
    public Text P2puntajeTexto;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        P1puntajeTexto.text = GameManager.P1puntos.ToString();
        P2puntajeTexto.text = GameManager.P2puntos.ToString();
    }
}
