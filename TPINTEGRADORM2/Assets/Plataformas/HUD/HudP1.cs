using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HudP1 : MonoBehaviour
{
    // RECURSOS HUD PLAYER1
    public GameObject Hud1;
    public Sprite A;
    public Sprite B;
    public Sprite C;
    public Sprite D;

    public GameObject Poder1;
    public GameObject Poder2;
    public Sprite P0;
    public Sprite P1;
    public Sprite P2;

    // RECURSOS HUD PLAYER2
    public GameObject Hud2;
    public Sprite A2;
    public Sprite B2;
    public Sprite C2;
    public Sprite D2;

    

    void Start()
    {

    }


    void Update()
    {
        ActualizarImagenesHud();
    }

    public void ActualizarImagenesHud()
    {
        switch (Jugador.Puntaje)
        {
            case 0:
                Hud1.GetComponent<Image>().sprite = A;
                break;
            case 1:
                Hud1.GetComponent<Image>().sprite = B;
                break;
            case 2:
                Hud1.GetComponent<Image>().sprite = C;
                break;
            case 3:
                Hud1.GetComponent<Image>().sprite = D;
                break;
        }

        switch (Jugador2.Puntaje)
        {
            case 0:
                Hud2.GetComponent<Image>().sprite = A2;
                break;
            case 1:
                Hud2.GetComponent<Image>().sprite = B2;
                break;
            case 2:
                Hud2.GetComponent<Image>().sprite = C2;
                break;
            case 3:
                Hud2.GetComponent<Image>().sprite = D2;
                break;
        }

        if (ControlNivel1.GanaRonda == 3)
        {
            Hud1.SetActive(false);
            Hud2.SetActive(false);
        }
        else
        {
            Hud1.SetActive(true);
            Hud2.SetActive(true);
        }

        switch (Jugador.Poder)
        {
            case 0:
                Poder1.GetComponent<Image>().sprite = P0;
                break;

            case 1:
                Poder1.GetComponent<Image>().sprite = P1;
                break;

            case 2:
                Poder1.GetComponent<Image>().sprite = P2;
                break;
        }

        switch (Jugador2.Poder)
        {
            case 0:
                Poder2.GetComponent<Image>().sprite = P0;
                break;

            case 1:
                Poder2.GetComponent<Image>().sprite = P1;
                break;

            case 2:
                Poder2.GetComponent<Image>().sprite = P2;
                break;
        }
    }
}
